.. _gsoc-site-editing-guide:

Site Editing Guide
##################

`GSoC <https://gsoc.beagleboard.io/>`_ site uses `OpenBeagle Continous Integration (CI) / Continous Development 
(CD) <https://docs.gitlab.com/ee/ci/>`_ which is a continuous method of software development, where 
you continuously build, test, deploy, and monitor iterative code changes. which means you don't have to setup 
anything on your local machine to update anything on the site. To contribute to this site, you can follow the 
simple steps provided below.

.. note:: `OpenBeagle <https://openbeagle.org/>`_ is a self hosted instance of open source program called `GitLab <https://about.gitlab.com/>`_.

.. tip:: 
    If you want to build on your local machine we have added ``venv-build-env.sh`` and ``requirements.txt`` 
    to help you setup sphinx and all the other dependencies. Execute these commands in your terminal,
    
    Install python modules and setup virtual environment,

    .. code:: shell

        . ./venv-build-env.sh

    Build and serve it live using ``sphinx-autobuild``,

    .. code:: shell

        make livehtml

    Now, you can open `http://127.0.0.1:8000 <http://127.0.0.1:8000>`_ on any browser to see the rendered HTML with live updates. 
    Clear cookies and site data in your browser window to view up to date site.

Fork the project
*****************

Go to `gsoc.beagleboard.io repo on OpenBeagle <https://openbeagle.org/gsoc/gsoc.beagleboard.io>`_ 
and click on fork button create a fork on your personal OpenBeagle profile.

.. image:: media/fork-button.png
    :align: center

After clicking on the fork button, you'll be taken to a page like shown below where you have to,

1. Select your profile from the dropdown.
2. Click on fork project button to initiate the forking process.

.. image:: media/fork-project.png
    :align: center

Select file to edit
*******************

After successfully forking the project you have to,

1. Make sure you are on the forked repo on your profile, it should be ``https://openbeagle.org/<user-name>/gsoc.beagleboard.io`` where <user-name> should be replaced with your OpenBeagle username.
2. Select any file you want to edit from the files & folders view of the repo page.

.. image:: media/repo-file-folders.png
    :align: center

After selecting the file you have to click on ``edit button`` and then choose either of the options from drop-down,

1. ``Open in Web IDE``, choose this if you want to work on multiple files.
2. ``Edit single file``, choose this if you want to make some small edits in a single file.

.. image:: media/edit-button.png
    :align: center

.. note:: 
    Choosing ``Web IDE`` will load a `Visual Studio Code Server <https://code.visualstudio.com/docs/remote/vscode-server>`_
    instance which is a feature rich source code editor. Depending on the internent connection your machine will take some to 
    load the editor and it can be a bit heavy for some machines to handle. Once fully loaded it should run smoothly but, if that is 
    not the case then please consider using single file editor option. Considering majority of the users will be using ``Web IDE`` option,
    we are using the ``Web IDE`` for the rest of this guide.

Start editing
*************

If you select to open your file in ``Web IDE`` you'll see a familar interface. The GitLab Web IDE is actually a rich
`Visual Studio Code Server <https://code.visualstudio.com/docs/remote/vscode-server>`_ hosted on OpenBeagle.

.. image:: media/ide.png
    :align: center

.. tip:: We use `reStructuredText (RST) <https://en.wikipedia.org/wiki/ReStructuredText>`_ for all of our documentation projects 
   including `this GSoC site  <https://gsoc.beagleboard.io/>`_ and `main docs site <https://docs.beagleboard.org/latest/>`_. 
   If you are new to reStructuredText you can checkout our `reStructuredText cheatsheet <https://docs.beagleboard.org/latest/
   intro/contribution/rst-cheat-sheet.html>`_ to get yourself familiar with reStructuredText. 
    
.. admonition:: Why not use Markdown for documentation? 
    
   Because reStructuredText stands out against Markdown as,

   1. It’s more fully-featured.
   2. It’s much more standardized and uniform.
   3. It has built-in support for extensions.

   For more detailed comparison you can checkout `this article on reStructuredText vs. Markdown for technical 
   documentation <https://eli.thegreenplace.net/2017/restructuredtext-vs-markdown-for-technical-documentation/>`_

Now you have to select a file and start editing. Below image shows some,

1. Edits made to the `ideas/index.rst` highlighted with green bar on left side of code editor window near line numbers.
2. Source control button indicating (1) file updated in the repo.

.. image:: media/make-edits.png
    :align: center

.. tip:: Just like you do on your normal Visual Studio Code desktop application, to commit your changes you can either 
    click on Source control ( :fa:`code-branch;pst-color-secondary` ) button or press ``CTRL + SHIFT + G`` to see all the edited fils.

After swithing to source control you have to,

1. Add a commit message in the input feild.
2. Click on ``Commit to main`` button or click on drop down button to choose ``Commit to new branch``.

.. image:: media/commit-changes.png
    :align: center

After clicking ``Commit to 'main'`` button you'll be prompted with a window (shown below) with three options,

1. ``Create new branch``
2. ``Cancel``
3. ``Continue``

Click on ``Continue`` button if you want to commit to main (default branch) if it's a single edit or commit. 
Click on ``Create new branch`` if you want to create a new branch and want to keep your main branch clean. Advantage 
of creating a new branch is that assigned reviewer for a pull request / merge request can also add commits to your 
newly created branch which is not possible for your main branch because it's a `protected branch <https://docs.gitlab.com/
ee/user/project/protected_branches.html>`_ by default.

.. image:: media/commit-branch.png
    :align: center

When all done right, the lower right side of the ``Web IDE`` you'll a prompt showing ``Success! Your changes have been committed`` message with 
two button,

1. ``Got to Project``
2. ``Continue working``

.. image:: media/commit-success.png
    :align: center

If you click on ``Go to Project`` button, you'll see the commit successfully applied as shown in the red box in the image below and the 
green tick shows on the right side indicates that the CI build was also successful.

.. image:: media/commit.png
    :align: center

.. admonition:: Congratulations!!
    
    You have made a valuable contribution to an OpenBeagle project!

Create a pull request
**********************

After making your changes and commiting them to your forked repo, you are set to create a new `pull request / merge request 
<https://en.wikipedia.org/wiki/Distributed_version_control#Pull_requests>`_ so that those changes can be merged to upstream 
repo. To start your new PR click on the dedicated button to create a new merge request and fill out all the details. The image 
below shows all the feilds you have to update,

1. Provided a good title that reflects your work.
2. Add a detailed description of the work you have done. Add pictures whenever seems useful.
3. (Optional) you can assign it to yourself if you'll be working on further updates or assign it to someone else who might want to work on the comments we may provide on your work.
4. Add ``lorforlinux`` or ``jkridner`` as reviewer for PRs with site content update or add your mentor as reviewer if it's related to project work / proposal.
5. If you want your commits to be shows as a single commit then you can choose the ``sqash commits ...`` check box.
6. Check if all your commits are shown in the bottom of the screen and if everything looks okay then click on ``Create merge request`` button.

.. tip:: If you are still working on some updates, you may also choose ``Mark as draft`` checkbox (below title) 
    which indicates that you are seeking feedback before making your commits suitable to merge.

.. image:: media/merge-request.png
    :align: center

Now wait for a review and, if comments are raised, then you can continue working on the project 
until everything looks perfect and your changes are merged in upstream.
