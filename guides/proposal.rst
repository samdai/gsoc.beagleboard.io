.. _gsoc-proposal-guide:

Proposal Guide 
###############

.. admonition:: Did you know?

    Each year Google turns down many more applicants than it funds. While pre-proposal activities are key to 
    improving your chances of success, a poorly-written proposal is an easy way to fail. There is much you can 
    do to ensure that your project proposal catches the attention of organization reviewers in a positive way.

How to write a successful proposal
***********************************

`BeagleBoard.org <https://www.beagleboard.org/>`_ has received a large number of GSoC contributor proposals over the years 
that we have participated. Some of these proposals are quite good, but many are poor proposals that would 
not be accepted by any organization participating in GSoC.

How to write an *unsuccessful* proposal
========================================

Unsuccessful proposals often:

1. Try to pass off previous school projects as something to do for GSoC. The work has already done, and the project often has very little to do with `BeagleBoard.org hardware <https://www.beagleboard.org/boards>`_,
2. Show up shortly before the submission deadline with only a half page of “proposal”, being little more than a name, contact information, and a statement of “I will work hard”,
3. Are generic and sent to every other GSoC org with very few details on deliverables or schedule, or
4. Introduce an idea that was never talked about to the `BeagleBoard.org <https://www.beagleboard.org/>`_ mentors in our `Forum <https://bbb.io/gsocml>`_ and submitted without ever engaging the Beagle community.

Please be considerate of the time it takes for us to weed out these bad proposals.

Elements of a successful proposal
==================================

In order to be successful, a proposal has to be:

1. Relevant to `BeagleBoard.org <https://www.beagleboard.org/>`_,
2. Around a topic that the mentors recognize from being discussed on our `Forum <https://bbb.io/gsocml>`_, possibly on our :ref:`gsoc-project-ideas` page, and sufficiently detailed that the mentors know exactly what you’ll be creating and when it will be done,
3. Shown to mentors via our `Forum <https://bbb.io/gsocml>`_ well before the submission deadline, so that the mentors can comment on it and offer suggestions that improve the proposal.

.. tip::
   Start your proposal early, speak with the mentors often, and dedicate an appropriate amount of time to both thinking 
   about the deliverables for your project and how you can describe those deliverables in writing.

Using proposal template
************************

To make the project proposal writing process easier for all the GSoC contributors we have created a :ref:`gsoc-proposal-template`.

.. tip:: 
    Start with :ref:`gsoc-site-editing-guide` to simplify the proposal writing process using OpenBeagle CI. It's always recommended to create a fork 
    of `gsoc.beagleboard.io repo on OpenBeagle <https://openbeagle.org/gsoc/gsoc.beagleboard.io>`_ and clone on your local machine. Instructions for 
    setting up Sphinx on local machine are also provided on :ref:`gsoc-site-editing-guide`. For any query you, can reach out on 
    our `Discord <https://bbb.io/gsocchat>`_ or `Forum <https://bbb.io/gsocml>`_.

You can either open your cloned `gsoc.beagleboard.io repo <https://openbeagle.org/gsoc/gsoc.beagleboard.io>`_ using Visual Studio Code on your local machine or use OpenBeagle Web IDE. Now, follow steps below to write your proposal,

1. Create a copy of ``gsoc.beagleboard.io/proposals/template.rst`` in ``gsoc.beagleboard.io/proposals`` folder. Rename the file to reflect your project (``project-name.rst``) or your initials (``<myname>.rst``). It's recommended to follow the structure and use a filename with all smallcase letters and without any space. 
2. Start editing your proposal, it should be straightforward if you already know reStructuredText. Don't worry if you are new to reStructuredText, checkout our `ReStructuredText Cheat Sheet <https://docs.beagleboard.org/latest/intro/contribution/rst-cheat-sheet.html>`_ which should guide you through writing your proposal using reStructuredText.
3. Commit your changes and push to your fork often. Share the commit links with mentors so that they can review your proposal drafts and provide their valuable feedback.
4. Check rendered version of your proposal on OpenBeagle pages, to view a rendered version of your updates you can go to ``<username>.beagleboard.org/<repo name>`` where ``username`` is your OpenBeagle username and ``<repo name>`` is the repository name you have selected while forking the project. For example, if username is ``lorforlinux`` and project name is ``gsoc.beagleboard.io`` the rendered site can be accessed live via `lorforlinux.beagleboard.io/gsoc.beagleboard.io <http://lorforlinux.beagleboard.io/gsoc.beagleboard.io>`_. 

.. tip:: 
    1. For quick review, you can share OpenBeagle pages link with mentors and community members on our `Discord <https://bbb.io/gsocchat>`_ or `Forum <https://bbb.io/gsocml>`_.
    2. On secondry (right) side bar you will see a **PDF download button** like shown below. Click that button on your proposal page to download PDF version of your proposal which you can submit directly on GSoC porposal submission portal.
    
       .. raw:: html

            <a role="button" href="">
                <i class="fa-solid fa-download"></i> Download PDF
            </a>
    

