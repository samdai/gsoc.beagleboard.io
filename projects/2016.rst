.. _gsoc-2016-projects:

:far:`calendar-days` 2016
#########################

Exposing the PRU as an I2C and SPI master Controller
*****************************************************

.. youtube:: fyaebfssmyE
   :width: 100%
      
| **Summary:** We could offload the task of bitbanging I2C,SPI,UART, to the PRU.The PRU is a separate core and,hence,not affected by the Linux Scheduler. Hence the motivation for exposing the PRU as an I2C,SPI,UART device comes from the fact that they would be no software overheads in doing it and hence,we would gain extra serial interfaces without wasting valuable CPU cycles in bitabanging.

**Contributor:** Vaibhav Choudhary

**Mentors:** Andrew Bradford, Matt Porter

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/projects/5936772366204928
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/chanakya-vc/PRU-I2C_SPI_master/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

SPI slave driver implementation
********************************

.. youtube:: yBgMwMcvcKg
   :width: 100%

| **Summary:** he task is to create a driver controlling SPI hardware controller in slave mode, and to ensure optimal performance through the use of DMA and interrupt. Creating an easy to implement realization of SPI slave would definitely help the BeagleBone community members to write applications based on SPI much more easily. 

**Contributor:** Patryk Mężydło

**Mentors:** Michael Welling, Andrew Bradford, Matt Porter

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/projects/6567958478323712
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/pmezydlo/SPI_slave_driver_implementation/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

BeagleScope
************

.. youtube:: tdanTRSmq4E
   :width: 100%

| **Summary:** This GSoC project aims to utilize the two PRUs present in the PRUSS2 subsystem and their low latency architecture to get a fast, generic, parallel analog converter interface, in the form of parallel bus, ready for various applications.

**Contributor:** Zubeen Tolani

**Mentors:** SJLC, Abhishek Kumar, Michael Welling, Hunyue Yau

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/projects/4885400476712960
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/ZeekHuge/BeagleScope/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

Porting the CTAG face2|4 multichannel soundcard drivers to BeagleBoard-X15 
*****************************************************************************

.. youtube:: u3A9x9bpbdo
   :width: 100%

| **Summary:** The `CTAG face2|4 Audio Card <http://www.creative-technologies.de/linux-based-low-latency-multichannel-audio-system-2/>`_ is a multichannel I2S soundcard based on the AD1938 audio codec by Analog Devices. Currently the soundcard can only be used with a `BeagleBone Green/Black <http://beagleboard.org/black>`_. Due to CPU intensive calculations in digital audio the `BeagleBoard-X15 <http://beagleboard.org/x15>`_ seems to be a perfect alternative to realize more complex audio effects and synthesizers and achieve lower latencies.

**Contributor:** Henrik Langer

**Mentors:** Robert Manzke, Vladimir Pantelic

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/projects/5351212496977920
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/henrix/libdsp-x15/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

Sonic Anemometer
*****************

.. youtube:: 1EHpIu2IRQA
   :width: 100%

| **Summary:** 

**Contributor:** Visaoni

**Mentors:** Alex Hiam, Micheal Welling, Kumar Abhishek, Deepak Karki

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/organizations/6012249034457088
         :color: light
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/Visaoni/beagle-sonic-anemometer/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

Improving Bone101 Experience
*****************************

.. youtube:: AdhE9wmNZww
   :width: 100%
      
| **Summary:** This idea is aimed to improve the experience of Bone101 to make the most use of it to be friendly to novice developers, allowing them to work with BBUI inside tutorials, write python code beside Javascript, and discover new experience of Bone101 in Cloud9 IDE.

**Contributor:** Your contributor information

**Mentors:** Your mentor information

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/projects/4505918104403968
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/amragaey/bone101/wiki/BeagleBoard-GSoC'16:-Improving-Bone101-Experience
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

API support for Beaglebone Blue
*******************************

| **Summary:** The aim of the project is to create easy-to-use APIs for the hardware on the BB Blue. This would consist of developing/improving kernel drivers for the on-board devices and then re-implementing the Strawson APIs to use these kernel drivers. These APIs will then be used by real-time applications which can be run on BB Blue. In the later phase of the project, support for BB Blue will be added in Ardupilot and ROS will be ported to BB Blue using these APIs.

**Contributor:** Kiran Kumar Lekkala

**Mentors:** Alex Hiam, Micheal Welling, Kumar Abhishek, Deepak Karki   

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2016/projects/6295262146330624
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/kiran4399/bb_blue_api/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

.. tip:: 

   Checkout eLinux page for GSoC 2016 projects `here <https://elinux.org/BeagleBoard/GSoC/2016_Projects>`_ for more details.
