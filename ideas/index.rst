:html_theme.sidebar_primary.remove: true
:sd_hide_title: true

.. _gsoc-project-ideas:

Ideas
######

.. image:: ../_static/images/ideas-below.webp
   :align: center

.. admonition:: How to participate?

   Contributors are expected to go through the list of ideas dropdown below and join the discussion by clicking on the
   ``Discuss on forum`` button. All ideas have colorful badges for ``Complexity`` and
   ``Size`` for making the selection process easier for contributors. Anyone is welcome to
   introduce new ideas via the `forum gsoc-ideas tag <https://forum.beagleboard.org/tag/gsoc-ideas>`_.
   Only ideas with sufficiently experienced mentor backing as deemed by the administrators will
   be added here.

   +------------------------------------+-------------------------------+
   | Complexity                         | Size                          |
   +====================================+===============================+
   | :bdg-danger:`High complexity`      | :bdg-danger-line:`350 hours`  |
   +------------------------------------+-------------------------------+
   | :bdg-success:`Medium complexity`   | :bdg-success-line:`175 hours` |
   +------------------------------------+-------------------------------+
   | :bdg-info:`Low complexity`         | :bdg-info-line:`90 hours`     |
   +------------------------------------+-------------------------------+

.. card:: Low-latency I/O RISC-V CPU core in FPGA fabric

   :fas:`microchip;pst-color-primary` FPGA gateware improvements :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`

   ^^^^

   BeagleV-Fire features RISC-V 64-bit CPU cores and FPGA fabric. In that FPGA fabric, we'd like to
   implement a RISC-V 32-bit CPU core with operations optimized for low-latency GPIO. This is similar
   to the programmable real-time unit (PRU) RISC cores popularized on BeagleBone Black.

   | **Goal:** RISC-V-based CPU on BeagleV-Fire FPGA fabric with GPIO
   | **Hardware Skills:** Verilog, Verification, FPGA
   | **Software Skills:** RISC-V ISA, assembly, `Linux`_
   | **Possible Mentors:** `Cyril Jean <https://forum.beagleboard.org/u/vauban>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/low-latency-risc-v-i-o-cpu-core/37156
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum


.. card:: Update beagle-tester for mainline testing

   :fab:`linux;pst-color-primary` Linux kernel improvements :bdg-success:`Medium complexity` :bdg-danger-line:`350 hours`

   ^^^^

   Utilize the ``beagle-tester`` application and ``Buildroot`` along with device-tree and udev symlink concepts within
   the OpenBeagle continuous integration server context to create a regression test suite for the Linux kernel
   and device-tree overlays on various Beagle computers.

   | **Goal:** Execution on Beagle test farm with over 30 mikroBUS boards testing all mikroBUS enabled cape interfaces (PWM, ADC, UART, I2C, SPI, GPIO and interrupt) performing weekly mainline Linux regression verification
   | **Hardware Skills:** basic wiring, familiarity with embedded serial interfaces
   | **Software Skills:** device-tree, `Linux`_, `C`_, continuous integration with GitLab, Buildroot
   | **Possible Mentors:** `Deepak Khatri <https://forum.beagleboard.org/u/lorforlinux>`_, `Anuj Deshpande <https://forum.beagleboard.org/u/Anuj_Deshpande>`_, `Dhruva Gole <https://forum.beagleboard.org/u/dhruvag2000>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/update-beagle-tester-for-cape-mikrobus-new-board-and-upstream-testing/37279
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: Upstream wpanusb and bcfserial

   :fab:`linux;pst-color-primary` Linux kernel improvements :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`
   
   ^^^^

   These are the drivers that are used to enable Linux to use a BeagleConnect Freedom as a SubGHz IEEE802.15.4 radio (gateway).
   They need to be part of upstream Linux to simplify on-going support. There are several gaps that are known before they are
   acceptable upstream.

   | **Goal:** Add functional gaps, submit upstream patches for these drivers and respond to feedback
   | **Hardware Skills:** Familiarity with wireless communication
   | **Software Skills:** `C`_, `Linux`_
   | **Possible Mentors:** `Ayush Singh <https://forum.beagleboard.org/u/ayush1325>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_

   ++++

   .. button-link:: https://forum.beagleboard.org/t/upstream-wpanusb-and-bcfserial/37186
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. card:: ``librobotcontrol`` support for newer boards

   :fas:`wand-sparkles;pst-color-danger` Automation and industrial I/O :bdg-success:`Medium complexity` :bdg-success-line:`175 hours`

   ^^^^

   Preliminary librobotcontrol support for BeagleBone AI, BeagleBone AI-64 and BeagleV-Fire has been drafted, but it
   needs to be cleaned up. We can also work on support for Raspberry Pi if UCSD releases their Hat for it.

   | **Goal:** Update librobotcontrol for Robotics Cape on BeagleBone AI, BeagleBone AI-64 and BeagleV-Fire
   | **Hardware Skills:** Basic wiring, some DC motor familiarity
   | **Software Skills:** `C`_, `Linux`_
   | **Possible Mentors:** `Deepak Khatri <https://forum.beagleboard.org/u/lorforlinux>`_, `Jason Kridner <https://forum.beagleboard.org/u/jkridner>`_
   
   ++++

   .. button-link:: https://forum.beagleboard.org/t/librobotcontrol-support-for-newer-boards/37187
      :color: danger
      :expand:

      :fab:`discourse;pst-color-light` Discuss on forum

.. button-link:: https://forum.beagleboard.org/tag/gsoc-ideas
   :color: danger
   :expand:
   :outline:

   :fab:`discourse;pst-color-light` Visit our forum to see newer ideas being discussed!


.. tip::
   
   You can also check our our :ref:`gsoc-old-ideas` and :ref:`Past_Projects` for inspiration.

.. _Linux:
   https://docs.beagleboard.org/latest/intro/beagle101/linux.html

.. _C:
   https://jkridner.beagleboard.io/docs/latest/intro/beagle101/learning-c.html
