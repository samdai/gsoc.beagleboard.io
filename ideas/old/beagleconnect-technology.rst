.. _gsoc-idea-beagleconnect-technology:

BeagleConnect Technology
########################

See https://github.com/beagleboard/beagleconnect. The BeagleConnect Freedom board 
runs the Zephyr RTOS has mikroBUS ports along with BLE and Sub-GHz radios on it. 
The code to enable a Linux gateway for the Sub-GHz radio is in the repository. 

.. card:: 

    :fas:`diagram-project;pst-color-secondary` **Rewrite GBridge**
    ^^^^

    Greybus was created to enable modular mobile phones. We are reusing it to drastically 
    simplify developing embedded systems. Fundamental to using Greybus for IoT (embedded 
    systems) is a tool called GBridge. GBridge is a combination of a kernel module called 
    gb-netlink and a userspace application called gbridge that enables routing of Greybus 
    traffic across various transports, including IP. It was initially written as a proof 
    of concept and has not been developed into a stable, production tool. It needs to be 
    rewritten with better error handling and, potentially, to be part of the Linux kernel 
    itself. Further, additional hooks, probably through debugfs, should be provided to 
    report on network status, including signal strength. This might further require 
    patches to the wpanusb and bcfserial drivers to expose additional debug information.

    - **Complexity:** 350 hours
    - **Goal:** GBridge should be submitted upstream, stable and provide debug visibility
    - **Hardware Skills:** Familiarity with wireless communication
    - **Software Skills:** C, Rust (optional)
    - **Possible Mentors:** jkridner, vaishnav
    - **Rating:** Medium
    - **Upstream Repository:** https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    - **References:**
        - https://github.com/jadonk/gbridge
        - https://github.com/beagleboard/beagleconnect
        - https://github.com/jadonk/wpanusb_bc
        - https://github.com/jadonk/bcfserial

    ++++

    :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`diagram-project;pst-color-secondary` **Zephyr SDK**
    ^^^^

    Beagle will be using Visual Studio Code as an IDE moving forward. We want to 
    simplify development targeting Zephyr by hosting a quality set of development 
    tools targeting BeagleConnect Freedom.

    - Complexity: 175 hours
    - Goal: Visual Studio Code environment hosted by Beagles ready to develop on Zephyr for BeagleConnect Freedom
    - Hardware Skills: flashing MCU boards
    - Software Skills: C, RTOS
    - Possible Mentors: jkridner, rcn-ee, lorforlinux
    - Rating: Medium
    - Upstream Repository: https://github.com/beagleboard/image-builder
    - References:
        - https://github.com/beagleboard/beagleconnect
        - https://github.com/jadonk/beagleconnect/blob/docker-arm-zephyrsdk/sw/zephyrproject/Dockerfile
        - https://github.com/rcn-ee/repos/tree/master/bbb.io-vsx-examples
        - https://github.com/rcn-ee/repos/tree/master/bb-code-server
        - https://github.com/beagleboard/Latest-Images

    ++++

    :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`diagram-project;pst-color-secondary` **Upstream wpanusb and bcfserial**
    ^^^^


    These are the drivers that are used to enable Linux to use a BeagleConnect Freedom as a 
    SubGHz IEEE802.15.4 radio (gateway). They need to be part of upstream Linux to simplify 
    on-going support. There are several gaps that are known before they are acceptable upstream

    Some notes from Stefan Schmidt:

    1. The wpanusb linux driver is missing quite a few driver ops (listen before talk, set frame retries). I added rough sketches but these are non functional due to 2.
    2. The zephyr firmware side needs to implement these ops as well and hook it up to the zephyr radio_api
    3. A way to read the permanent extended address from the device to use in Linux is needed
    4. The generic aspect of the firmware driver combo is missing completely. Band, supported channel, power levels, etc is all hard-coded in the linux driver and not queried from the device firmware. Its not even giving the page on which a channel is being set when changing it.
    5. More driver ops are coming in right now as there is heavy work towards support management frames as well as active and passive scanning on the Linux stack. Which means these would need proper handling on the firmware side as well.

    - Complexity: 175 hours
    - Goal: Submit upstream patches for these drivers and respond to feedback
    - Hardware Skills: Familiarity with wireless communication
    - Software Skills: C, Linux
    - Possible Mentors: jkridner, vaishnav
    - Rating: Medium
    - Upstream Repository: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    - References:
        - https://www.kernel.org/doc/html/latest/process/submitting-patches.html
        - https://github.com/beagleboard/beagleconnect
        - https://github.com/jadonk/wpanusb_bc
        - https://github.com/jadonk/bcfserial

    ++++

    :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 
    
    :fas:`diagram-project;pst-color-secondary` **BeagleConnect(Greybus) host on WIO Terminal**
    ^^^^

    - **Complexity:** 350 hours
    - **Goal:** Enable remote access to mikroBUS(GPIO, I2C, SPI, PWM, ADC drivers) via UART/BLE.
    - **Hardware Skills:** Basic wiring
    - **Software Skills:** C, RTOS
    - **Possible Mentors:** jkridner, vaishnav
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/zephyrproject-rtos/zephyr/pull/40811
    - **References:**
        - https://github.com/zephyrproject-rtos/zephyr
        - https://github.com/beagleboard/beagleconnect
        - https://beagleboard.org/p/ansonhe1997/how-to-use-wio-terminal-as-a-usb-hmi-for-beagleboard-sbc-78e6b9
        - https://elinux.org/BeagleBoard/GSoC/2021_Proposal/MicroPython_with_GreyBus_for_BeagleConnect_Freedom

    ++++

    :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`diagram-project;pst-color-secondary` **Greybus for Zephyr Updates**
    ^^^^

    - **Complexity:** 350 hours
    - **Goal:** Support all the relevant peripherals(SPI, UART, PWM, ADC, GPIO IRQ) on BeagleConnect Freedom on Greybus for Zephyr, this requires adding platform specific changes to enable the protocols and interface to the existing NuttX sources, once the protocols are tested over greybus-for-zephyr, the mikrobus kernel driver needs to be updated to be able to support mikrobus add-on boards on these buses. An ideal end goal for the project would be to test one mikrobus add-on board of each category over greybus with the corresponding device driver in kernel.
    - **Hardware Skills:** Basic wiring
    - **Software Skills:** C, Linux Kernel, RTOS
    - **Possible Mentors:** Vaishnav, jkridner
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/jadonk/greybus-for-zephyr
    - **References:** 
        - https://github.com/RobertCNelson/bb-kernel/tree/am33x-v5.17/patches/drivers/mikrobus
        - https://github.com/jadonk/beagleconnect

    ++++

    :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`diagram-project;pst-color-secondary` **Micropython for BeagleConnect Freedom**
    ^^^^

    - Goal: Enable Micropython for BeagleConnect Freedom along with basic GPIO, I2C, SPI, PWM, ADC drivers.
    - Hardware Skills: Basic wiring
    - Software Skills: C, RTOS
    - Possible Mentors: jkridner, Vaishnav, lorforlinux
    - Expected Size of Project: 350 hrs
    - Rating: Medium
    - Upstream Repository: https://github.com/micropython/micropython/tree/master/ports/zephyr
    - References: 
        - https://github.com/zephyrproject-rtos/zephyr
        - https://github.com/jadonk/beagleconnect

    ++++

    :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 